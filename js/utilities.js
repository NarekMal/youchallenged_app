module.exports = {
  showModal: function($scope, $ionicModal, templateUrl, backButton) {
      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: $scope,
        hardwareBackButtonClose: backButton
      }).then(function (modal) {
        $scope.Modal = modal;
        $scope.Modal.show();
      });
    },
  showLoading: function($ionicLoading){
      $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
    },
  hideLoading: function($ionicLoading){
      $ionicLoading.hide();
    },
  responseIsOk: function(response){
      return response == 'ok';
    }
}







