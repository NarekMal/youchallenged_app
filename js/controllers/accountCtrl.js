utilities = require('./../utilities.js');

module.exports = function ($scope,$rootScope,$state,$ionicHistory,$ionicLoading,$ionicActionSheet,$timeout,userService,challengeService) {
	var imgProfile;
	$scope.accauth = JSON.parse(localStorage.getItem('authentication'));
	utilities.showLoading($ionicLoading);
	userService.getProfile($scope.accauth).success(function(response){
		if(response.status==true){
			response.user_image=response.user_image.replace(/\"/g,"");
			response.user_image=response.user_image.replace(/\'/g,"");
			imgProfile=response.user_image.match(/\?src=https(.*?)\&/);
			if(imgProfile==null){
				imgProfile=response.user_image.match(/src=https(.*?)\&/);
			}
			response.user_image="https"+imgProfile[1];
			$scope.acc=response;

      $scope.inputModels = {
        userName: $scope.acc.user_display_name,
        userEmail: $scope.acc.user_email
      };

      // Load category info if not already loaded
      if(!$rootScope.yChallengeCategory)
        challengeService.getYChallengeCategoryInfo($scope.accauth).success(function(resp){
          $rootScope.yChallengeCategory = resp.category_name;
          $rootScope.yChallengesInCategory = resp.all_challenges;
          $rootScope.complYChallengesInCategory = resp.completed_challenges;
          $rootScope.yChallengeBadgeUrl = resp.category_badge_url;
          utilities.hideLoading($ionicLoading);
        });
      else
        utilities.hideLoading($ionicLoading);
      //$scope.yCatName = getCategoryNameById (response.selected_challenge_category, response.challenge_categories);
      // $scope.yCatProgress = response.selected_challenge_category_progress;
      // if ($scope.yCatProgress == 1)
      //   $scope.yCatBadgeURL = response.challenge_category_badge_url;
      // $scope.challengeCategories = response.challenge_categories;
			// $ionicLoading.hide();
		}
	});

  $scope.buy = function () {
    var productId;
    var subscriptionMethod;
    switch(ionic.Platform.platform())
    {
      case 'ios':
        productId = 1;
        subscriptionMethod = 'Apple';
        break;
      case 'android':
        productId = 2;
        subscriptionMethod = 'Android';
        break;
      default:
        $scope.failedToSubscribe = true;
        return;
    }
    $scope.isProcessing = true;

    inAppPurchase
      .buy('monthly_plan')
      .then(function (data) {
        console.log(JSON.stringify(data));
        console.log('consuming transactionId: ' + data.transactionId);
        //return inAppPurchase.consume(data.type, data.receipt, data.signature);
        $scope.isProcessing = false;
        $scope.justSubscribed = true;
        $scope.acc.has_active_plan = true;
        $scope.acc.plan_subscription_method = subscriptionMethod;
      })
      // .then(function () {
      // })
      .catch(function (err) {
        console.log(err);
        $scope.isProcessing = false;
        $scope.failedToSubscribe = true;
      });

  };

  $scope.changeChallengeCategory = function (selectedCategory) {
    $scope.isYCatChanging = true;
    $scope.yCatName = getCategoryNameById(selectedCategory, $scope.challengeCategories);
    $scope.yCatProgress = 0;
    userService.changeYChallengeCategory(selectedCategory, $scope.accauth).success(function(response){
      $scope.isYCatChanging = false;
      $scope.yCatProgress = response.progress;
      if ($scope.yCatProgress == 1)
        $scope.yCatBadgeURL = response.challenge_category_badge_url;
    });
  }

	$scope.logout = function(){
		localStorage.setItem("headContent","");
		localStorage.removeItem("headContent");
		localStorage.setItem("Fashion","");
		localStorage.removeItem("Fashion");
		localStorage.setItem("Hair and Makeup","");
		localStorage.removeItem("Hair and Makeup");
		localStorage.setItem("Relationships","");
		localStorage.removeItem("Relationships");
		localStorage.setItem("Life Coaching","");
		localStorage.removeItem("Life Coaching");
		localStorage.setItem("Fitness","");
		localStorage.removeItem("Fitness");
		localStorage.setItem("Interior design","");
		localStorage.removeItem("Interior design");
		localStorage.setItem("rewards","");
		localStorage.removeItem("rewards");
		localStorage.setItem("homeVisit","");
		localStorage.removeItem("homeVisit");
		localStorage.removeItem('authentication');
		$ionicHistory.clearCache();
		$ionicHistory.clearHistory();
		$ionicHistory.nextViewOptions({
			disableBack: true
		});
		$state.go('login');
	}

  // User name and email updating
  $scope.processInputKeyPress = function(e){
    if(e.key == 'Escape'){
      $scope.inputModels.userName = $scope.acc.user_display_name;
      $scope.inputModels.userEmail = $scope.acc.user_email;
    }
    if(e.key == 'Enter')
      $scope.updateUserNameEmail();
  }

  $scope.updateUserNameEmail = function(){
    if ($scope.inputModels.userName == $scope.acc.user_display_name && $scope.inputModels.userEmail == $scope.acc.user_email)
      return;

    utilities.showLoading($ionicLoading);
    userService.setUserNameEmail($scope.inputModels.userName, $scope.inputModels.userEmail, $scope.accauth).success(function(response){
      utilities.hideLoading($ionicLoading);
      if (utilities.responseIsOk(response)){
        $scope.acc.user_display_name = $scope.inputModels.userName;
        $scope.acc.user_email = $scope.inputModels.userEmail;
      }
      else
        alert("Failed to update");
    });
  }

  $scope.showPlanInfo = function(){
    $scope.planInfoIsShown = true;
  }

  $scope.updatePicture = function () {

 		var options = {
			quality: 50,
			destinationType: 0,
			allowEdit: true,
			saveToPhotoAlbum: false,
			correctOrientation:true
		};
		var hideSheet = $ionicActionSheet.show({
			buttons: [
				{
					text: 'Gallery'
				},
				{
					text: 'Camera'
				}
			],
			titleText: 'Image Source',
			cancelText: 'Cancel',
			buttonClicked: function(index) {
				if(index == 0) {
				  options.sourceType = 0;
				} else {
				  options.sourceType = 1;
				}
        Camera.getPicture(options).then(function(imageData) {
					var img = "data:image/jpeg;base64," +imageData;
					utilities.showLoading($ionicLoading);
					userService.setPhoto({
            user_avatar:img
					},$scope.accauth).success(function (){
						utilities.hideLoading($ionicLoading);
						$scope.getacc();
					}) ;
				}, function(err){
					alert(JSON.stringify(err));
				});
			}
		});
		$timeout(function() {
			hideSheet();
		}, 3000);
	};

};
