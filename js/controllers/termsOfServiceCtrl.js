utilities = require('./../utilities.js');

module.exports = function ($scope,$state,$ionicLoading,userService) {

  $auth = JSON.parse(localStorage.getItem('authentication'));

  var init = function(){
    utilities.showLoading($ionicLoading);
    userService.getAcceptedTOS($auth).success(function(resp){
      $scope.accepted = resp;
      utilities.hideLoading($ionicLoading);
      console.log(resp);
    });
  };
  init();

  $scope.accept = function(){
    utilities.showLoading($ionicLoading);
    userService.setAcceptedTOS($auth).success(function(resp){
      if(utilities.responseIsOk(resp))
        init();
      else{
        utilities.hideLoading($ionicLoading);
        alert('Error occurred');
      }
    });
  }

  $scope.handleContinueClick = function(){
    if($scope.accepted)
      $state.go('tabsController.account');
  }

};
