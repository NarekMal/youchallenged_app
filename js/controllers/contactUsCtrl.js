utilities = require('./../utilities.js');

module.exports = function ($scope,$ionicLoading,$ionicPopup,userService) {

  $auth = JSON.parse(localStorage.getItem('authentication'));

  $scope.inputData = {};

  $scope.handleSendClick = function(){
    console.log($scope.inputData.name);
    console.log($scope.inputData.email);
    console.log($scope.inputData.text);
    utilities.showLoading($ionicLoading);
    userService.handleContactFormData($scope.inputData.name, $scope.inputData.email, $scope.inputData.text, $auth).success(function(resp){
      utilities.hideLoading($ionicLoading);
      if (utilities.responseIsOk(resp))
        $ionicPopup.alert({template: 'Message sent'});
      else
        alert('Error occurred');
      console.log(resp);
    });
  };

};
