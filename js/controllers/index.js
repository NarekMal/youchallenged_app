utilities = require('./../utilities.js');

angular.module('app.controllers', [])
.controller('introCtrl',function ($scope, $rootScope) {
	localStorage.setItem("homeVisit","");
	localStorage.removeItem("homeVisit");
	$rootScope.homeVisit = true;
	$scope.slide=[];
	$scope.slide[1]="fadeOut";
	$scope.slide[2]="fadeOut";
	$scope.slideHasChanged=function(index){
		if(index==0)
			$scope.slide[index+1]="fadeOut";
		else if(index==2)
			$scope.slide[index-1]="fadeOut";
		else{
			$scope.slide[index-1]="fadeOut";
			$scope.slide[index+1]="fadeOut";
		}
		$scope.slide[index]="fadeIn";
	}
})
.controller('loginCtrl',function ($scope,$state,$ionicHistory,$ionicLoading,userService) {
	$scope.data = {};

	$scope.login = function(formdata){
		if(!formdata){
			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			$scope.auth = btoa($scope.data.email+':'+$scope.data.password);
			localStorage.setItem('authentication', JSON.stringify($scope.auth));
			$scope.authen = JSON.parse(localStorage.getItem('authentication'));
			userService.login($scope.authen).success(function(rest){
				if (rest.is_user_logged_in==true)
				{
					$ionicHistory.clearHistory();
					$ionicHistory.nextViewOptions({
						disableBack: true
					});
					$state.go('tabsController.home');
					$ionicLoading.hide();
				}
				else{
					$ionicLoading.hide();
				}
			})
		}
	}
})
.controller('contestCtrl',function ($scope,$state,$ionicHistory,$ionicLoading,userService) {
	$scope.data = {};
})
.controller('rulesCtrl',function ($scope,$state,$ionicHistory,$ionicLoading,userService) {
	$scope.data = {};
})
.controller('forgetpassCtrl',function ($scope,$ionicHistory,userService,$ionicPopup,$ionicLoading){
	$scope.data={};
	$scope.forgetpass=function(formdata){
		if(!formdata){
			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			userService.resetpassword({
				user_login :$scope.data.email
			}).then(function (res){
				if(res.data == 'done')
				{
					$ionicPopup.alert({
						title: 'Information',
						template: 'Done'});
					$ionicLoading.hide();
					$ionicHistory.goBack();
				}
				else
				{
					alert(res.data);
				}
			})
		}
	}
})
.controller('signupCtrl',function ($scope,$ionicHistory,$ionicLoading,$ionicPopup,userService) {
	$scope.user = {};
	$scope.register = function(formdata){
		if(!formdata){
			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			userService.create({
				user_name:$scope.user.username,
				email:$scope.user.email,
				pass1:$scope.user.password,
				pass2:$scope.user.password_verify
			}).success(function(rest){
				if(rest == 'done'){
					$ionicPopup.alert({
						title: 'Information',
						template: 'Done'});
					$ionicLoading.hide();
					$ionicHistory.goBack();
				}else{
					alert(rest);
					$ionicLoading.hide();
				}
			})
		}
	}
})
.controller('homeCtrl', require('./homeCtrl'))
.controller('yChallengesCtrl', require('./yChallengesCtrl'))
.controller('yChallengeCtrl', require('./yChallengeCtrl'))
.controller('yChallengeAcceptedCtrl', require('./yChallengeAcceptedCtrl'))
.controller('messagesCtrl', require('./messagesCtrl'))
.controller('termsOfServiceCtrl', require('./termsOfServiceCtrl'))
.controller('contactUsCtrl', require('./contactUsCtrl'))
.controller('messagecategoryCtrl', function($scope,$ionicHistory,$sce,$state,$ionicModal,$stateParams,messageService,$ionicLoading) {
	$scope.goBack=function(){
		if($stateParams.From==="messagetitle")
			$ionicHistory.goBack();
		else
			$state.go($stateParams.From, {}, {reload:true});
	};
	$scope.data=[];$scope.mc=[];var t=0;
	$scope.messageauth = JSON.parse(localStorage.getItem('authentication'));
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});
	$scope.statusGoal=false;
	messageService.catMessage($stateParams.param1,$scope.messageauth).success(function(response){
		if(response.data.posts.length>0){
			coachImg=response.data.user_avatar_fetch_avatar.match(/src=(.*?)&/);
			$scope.fash = response.data;
			$scope.fash.user_avatar_fetch_avatar=coachImg[1];
			$scope.fash.quote=$scope.fash.quote.replace(/\"/g,'');
			angular.forEach(response.data.posts, function(Data) {
				$scope.mc.push(Data);
				$scope.mc[t].the_title=$sce.trustAsHtml($scope.mc[t].the_title);t++;
			});
			messageService.getGoal($stateParams.param1,$scope.messageauth).success(function(response1){
				$scope.statusGoal=response1.status;
				if(response1.status){
					$scope.messageGoal=response1.data.text;
					$scope.data.goal=response1.data.text;
				}
				$scope.goalID=response1.data.id;
			})
		}
		$ionicLoading.hide();
	});
	$scope.favorites = function(id){
		messageService.setFavourite({message_id:id},$scope.messageauth).then(function(){
			$state.reload();
		});
	};
	$scope.set=function(){
		showModal($scope, $ionicModal, 'goal.html', true);
	};
	$scope.closeModal = function(){
		$scope.Modal.hide();
		$scope.Modal.remove();
	};
	$scope.add=function(){
		if($scope.data.goal){
			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			if($scope.statusGoal){
				messageService.deleteGoal({
					id : $scope.goalID
				},$scope.messageauth).success(function(rest1){
					messageService.addGoal({
						goal : $scope.data.goal,
						category : $stateParams.param1
					},$scope.messageauth).success(function(rest2){
						messageService.getGoal($stateParams.param1,$scope.messageauth).success(function(response1){
							$scope.statusGoal=response1.status;
							$scope.messageGoal=response1.data.text;
						});
						$ionicLoading.hide();
						$scope.closeModal();
					})
				});
			}else{
				messageService.addGoal({
					goal : $scope.data.goal,
					category : $stateParams.param1
				},$scope.messageauth).success(function(rest){
					messageService.getGoal($stateParams.param1,$scope.messageauth).success(function(response1){
						$scope.statusGoal=response1.status;
						$scope.messageGoal=response1.data.text;
					});
					$ionicLoading.hide();
					$scope.closeModal();
				})
			}
		}
	};
	$scope.statusSelfGoal=function(id){
		messageService.statusGoal({
			id : id
		},$scope.messageauth).success(function(rest1){
			messageService.deleteGoal({
				id : id
			},$scope.messageauth).success(function(rest2){
				messageService.getGoal($stateParams.param1,$scope.messageauth).success(function(response1){
					$scope.statusGoal=response1.status;
					$scope.data.goal="";
				})
			});
		});
	}
})
.controller('messagetitleCtrl', function($scope,$ionicLoading,$ionicHistory,$state,$cordovaSocialSharing,messageService,$stateParams, $sce){
	$scope.messageauth = JSON.parse(localStorage.getItem('authentication'));
	$scope.goBack=function(){
		if($stateParams.From=="messagecategory")
			$ionicHistory.goBack();
		else
			$state.go($stateParams.From, {}, {reload:true});
	};
	$scope.oneshow=[];
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});
	messageService.titleMessage($stateParams.param1,$scope.messageauth).success(function(res){
		var firstContent="",secondContent="",position;
		var msgContent1=res.post.message_content.match(/<(.|\n)*?>/g) || [];
		res.post.url="";
		$scope.videoShow=false;
		for(m=0;m<msgContent1.length;m++){
			if(msgContent1[m].indexOf("<iframe src=")>=0){
				position=m;
				var msgContent2=msgContent1[position].match(/<iframe src=\"(.*?)\" width=\"/);
				res.post.url=msgContent2[1];
				$scope.videoShow=true;
			}else{
				if(m>position){
					secondContent=secondContent+msgContent1[m];
				}else{
					firstContent=firstContent+msgContent1[m];
				}
			}
		}
		res.post.first_content=$sce.trustAsHtml(res.post.message_content);
		res.post.second_content=secondContent;
		$scope.oneshow = res.post;
		console.log(res.post.first_content);
		console.log('----');
		console.log(res.post.second_content);
		$scope.oneshow.challenge_is=res.post.challenge_is;
		$ionicLoading.hide();
	});
	$scope.setChallenge = function(id,load){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		messageService.setChallenge({message_id : id},$scope.messageauth).success(function(response){
			$ionicLoading.hide();
			$ionicHistory.nextViewOptions({
				disableBack: true
			});
			$state.go('tabsController.challenges',{},{reload:true});
		})
	};
	$scope.share=function(linkM){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		$cordovaSocialSharing.shareViaFacebook(linkM).then(function(result) {
			$ionicLoading.hide();
		}, function(err) {
			console.log('facebook fail');
		});
	}
})
.controller('challengesCtrl',function ($scope,$ionicHistory,challengeService,$state,$ionicLoading,$ionicPopup) {
	$scope.challengeauth = JSON.parse(localStorage.getItem('authentication'));
	$scope.response_challenge ='';
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});
	challengeService.getChallenge($scope.challengeauth).success(function(rest){
		$ionicLoading.hide();
		$scope.challenge = rest;
		$scope.challenge.score=rest.percentage;
		if(rest.isset_new_challenge_camp_id == true){
			$scope.accepted= rest.new_challenges;
		}else{
			alert("No Challenges");
		}
	});
	$scope.pr=[];
	$scope.show = function(id){
		delete $scope.response_challenge;
		delete $scope.challege_id;
		$ionicPopup.confirm({
			title: 'Confirmation',
			template: 'Are you sure?'
		}).then(function(res) {
			if(res) {
				$ionicLoading.show({
					content: 'Loading',
					animation: 'fade-in',
					showBackdrop: true,
					maxWidth: 200,
					showDelay: 0
				});
				challengeService.completeChallenge({
					message_id : id
				},$scope.challengeauth).success(function(response){
					$scope.pr.push(id);
					$scope.response_challenge = response;
					$scope.challege_id = id;
					$scope.isFavorites = function(id) {
						return $scope.pr.indexOf(id) !== -1;
					};
					challengeService.getChallenge($scope.challengeauth).success(function(rest){
						$ionicLoading.hide();
						$scope.challenge = rest;
						$scope.challenge.score=rest.compl_chall_count/(rest.acc_challenges_count+rest.compl_chall_count)*100;
						if(rest.isset_new_challenge_camp_id == true){
							$scope.accepted= rest.new_challenges;
						}else{
							alert("No Challenges");
						}
					});
				})
			} else {
				$scope.response_challenge = '';
				$scope.challege_id='';
			}
		})
	};
	$scope.messageTitle=function(ID){
		$state.go('messagetitle',{From:"tabsController.challenges",param1:ID},{reload:true});
	}
})
.controller('completedChallengesCtrl', function($scope,$ionicHistory,$sce,challengeService,$ionicLoading) {
	$scope.completeauth = JSON.parse(localStorage.getItem('authentication'));
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});
	$scope.co=[];
	challengeService.specChallenge($scope.completeauth).success(function(res){
		angular.forEach(res.posts, function(Data) {
			Data.the_title=$sce.trustAsHtml(Data.the_title);
			$scope.co.push(Data);
		});
		$ionicLoading.hide();
	})
})
.controller('accountCtrl', require('./accountCtrl'))
.controller('editprofileCtrl',function ($scope,$ionicHistory,$timeout,$ionicLoading,userService,$rootScope,$ionicPopup,$ionicActionSheet, Camera) {
	var imgProfile;
	$scope.accauth = JSON.parse(localStorage.getItem('authentication'));
	$scope.getacc=function(){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		userService.getProfile($scope.accauth).success(function(response){
			if(response.status==true){
				$scope.user.firstname=response.user_name;
				$scope.user.email=response.user_email;
				response.user_image=response.user_image.replace(/\"/g,"");
				response.user_image=response.user_image.replace(/\'/g,"");
				imgProfile=response.user_image.match(/\?src=https(.*?)\&/);
				if(imgProfile==null){
					imgProfile=response.user_image.match(/src=https(.*?)\&/);
				}
				response.user_image="https"+imgProfile[1];
				$scope.editt=response;
				$ionicLoading.hide();
			}
		});
	};
	$scope.getacc();
	$scope.pictureupdate = function () {

 		var options = {
			quality: 50,
			destinationType: 0,
			allowEdit: true,
			saveToPhotoAlbum: false,
			correctOrientation:true
		};
		var hideSheet = $ionicActionSheet.show({
			buttons: [
				{
					text: 'Gallery'
				},
				{
					text: 'Camera'
				}
			],
			titleText: 'Image Source',
			cancelText: 'Cancel',
			buttonClicked: function(index) {
				if(index == 0) {
				  options.sourceType = 0;
				} else {
				  options.sourceType = 1;
				}
        Camera.getPicture(options).then(function(imageData) {
					var img = "data:image/jpeg;base64," +imageData;
					$ionicLoading.show({
						content: 'Loading',
						animation: 'fade-in',
						showBackdrop: true,
						maxWidth: 200,
						showDelay: 0
					});
					userService.setPhoto({
            user_avatar:img
					},$scope.accauth).success(function (){
						$ionicLoading.hide();
						$scope.getacc();
					}) ;
				}, function(err){
					alert(JSON.stringify(err));
				});
			}
		});
		$timeout(function() {
			hideSheet();
		}, 3000);
	};
	$scope.user = {};
	$scope.editprofile = function(){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		userService.setProfile({
			first_name:$scope.user.firstname,
			last_name:$scope.user.lastname,
			email:$scope.user.email,
		},$scope.accauth).success(function (res){
			$ionicLoading.hide();
			$ionicHistory.goBack();
		})
	}
})
.controller('changepasswordCtrl',function ($scope,userService,$ionicHistory,$rootScope,$state,$ionicLoading) {
	$scope.passauth = JSON.parse(localStorage.getItem('authentication'));
	$scope.user ={};
	$scope.changepassword = function(){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		userService.setProfile({
			pass1:$scope.user.password,
			pass2:$scope.user.rpassword
		},$scope.passauth).success(function (rest){
			$ionicLoading.hide();
			localStorage.removeItem('authentication');
			$ionicHistory.clearCache();
			$ionicHistory.clearHistory();
			$ionicHistory.nextViewOptions({
				disableBack: true
			});
			$state.go('login');
		})
	}
})
.controller('q1Ctrl', function($scope,quizService,$state,$rootScope){
	$scope.ready=false;$rootScope.allquiz=[];
	quizService.getQuestion().success(function(rest){
		var i=0;
		angular.forEach(rest, function(Data) {
			$rootScope.allquiz.push(Data);i++;
			if(i==rest.length)
				$scope.ready=true;
		});
	})
})
.controller('q2Ctrl', function($scope,quizService,$state,$rootScope){
	$rootScope.q2=$rootScope.allquiz[0];
	$rootScope.qimage=$rootScope.allquiz[0].answers;
	$scope.data={};
	$scope.quizauth = JSON.parse(localStorage.getItem('authentication'));
	$scope.xyz=[];
	var defaultColor=-1;
	$scope.answer=function(index){
		if(defaultColor>=0){
			$scope.xyz[defaultColor]={border:"2px #ccc solid"};
		}
		$scope.xyz[index]={border:"2px #ffc900 solid"};
		defaultColor=index;
	};
	$scope.quiz=function(){
		if(defaultColor==0){
			alert("We're coaching only women at the moment, but plan to add men.");
		}else if(defaultColor==1){
			quizService.setAnswer({
				quest:$rootScope.q2.input_name,
				answer:$rootScope.qimage[defaultColor].value
			},$scope.quizauth).then(function(res){
				if(res.statusText == 'OK'){
					$state.go("q3");
				}else{
					console.log("wronnnnnng");
				}
			})
		}else{
			alert("Select an answer");
		}
	}
})
.controller('q3Ctrl', function($scope,quizService,$state,$rootScope){
	$rootScope.q3=$rootScope.allquiz[1];
	$rootScope.question=$rootScope.allquiz[1].answers;
	console.log($rootScope.question);
	for(d=0;d<$rootScope.allquiz[1].answers.length;d++){
		$rootScope.question[d].selection=false;
	}
	$scope.q3auth = JSON.parse(localStorage.getItem('authentication'));
	$scope.xyz=[];
	var optAnswer={
		"idx":[0,0,0,0,0],
		"name":["fash_bohemian","fash_chic","fash_classic","fash_rocker","fash_tomboy"]
	},add,indexOfMax;
	$scope.answer=function(index){
		if($rootScope.question[index].selection){
			$scope.xyz[index]={border:"2px #ccc solid"};
			$rootScope.question[index].selection=false;add=-1;
		}else{
			$scope.xyz[index]={border:"2px #ffc900 solid"};
			$rootScope.question[index].selection=true;add=1;
		}
		if($rootScope.question[index].value=="fash_bohemian")
			optAnswer.idx[0]=optAnswer.idx[0]+add;
		else if($rootScope.question[index].value=="fash_chic")
			optAnswer.idx[1]=optAnswer.idx[1]+add;
		else if($rootScope.question[index].value=="fash_classic")
			optAnswer.idx[2]=optAnswer.idx[2]+add;
		else if($rootScope.question[index].value=="fash_rocker")
			optAnswer.idx[3]=optAnswer.idx[3]+add;
		else
			optAnswer.idx[4]=optAnswer.idx[4]+add;
		indexOfMax = optAnswer.idx.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);
	};
	$scope.quiz=function(){
		if(optAnswer.idx[indexOfMax]>0){
			quizService.setAnswer({
				quest:$rootScope.q3.input_name,
				answer:optAnswer.name[indexOfMax]
			},$scope.q3auth).then(function(res){
				if(res.statusText == 'OK')
				{
					$state.go("q4");
				}
				else
				{
					console.log("wronnnnnng");
				}
			})
		}else{
			alert("Select an answer");
		}
	}
})
.controller('q4Ctrl', function($scope,quizService,$state,$rootScope){
	$rootScope.q4=$rootScope.allquiz[2];
	$rootScope.questionn=$rootScope.allquiz[2].answers;

	$scope.q4auth = JSON.parse(localStorage.getItem('authentication'));
	$scope.xyz=[];
	var defaultColor=-1;
	$scope.answer=function(index){
		if(defaultColor>=0){
			$scope.xyz[defaultColor]={border:"2px #ccc solid"};
		}
		$scope.xyz[index]={border:"2px #ffc900 solid"};
		defaultColor=index;
	};
	$scope.quiz=function(){
		if(defaultColor>=0){
			quizService.setAnswer({
				quest:$rootScope.q4.input_name,
				answer:$rootScope.questionn[defaultColor].value
			},$scope.q4auth).then(function(res){
				if(res.statusText == 'OK')
				{
					$state.go("q5");
				}
				else
				{
					console.log("wronnnnnng");
				}
			})
		}else{
			alert("Select an answer");
		}
	}
})
.controller('q5Ctrl', function($scope,quizService,$state,$rootScope){
	$rootScope.q5=$rootScope.allquiz[3];
	$rootScope.questioni=$rootScope.allquiz[3].answers;

	$scope.q5auth = JSON.parse(localStorage.getItem('authentication'));
	$scope.xyz=[];
	var defaultColor=-1;
	$scope.answer=function(index){
		if(defaultColor>=0){
			$scope.xyz[defaultColor]={border:"2px #ccc solid"};
		}
		$scope.xyz[index]={border:"2px #ffc900 solid"};
		defaultColor=index;
	};
	$scope.quiz=function(){
		if(defaultColor>=0){
			quizService.setAnswer({
				quest:$rootScope.q5.input_name,
				answer:$rootScope.questioni[defaultColor].value
			},$scope.q5auth).then(function(res){
				if(res.statusText == 'OK')
				{
					$state.go("q6");
				}
				else
				{
					console.log("wronnnnnng");
				}
			})
		}else{
			alert("Select an answer");
		}
	}
})
.controller('q6Ctrl', function($scope,quizService,$state,$rootScope){
	$rootScope.q6=$rootScope.allquiz[4];
	$rootScope.questionie=$rootScope.allquiz[4].answers;

	$scope.q6auth = JSON.parse(localStorage.getItem('authentication'));
	$scope.xyz=[];
	var defaultColor=-1;
	$scope.answer=function(index){
		if(defaultColor>=0){
			$scope.xyz[defaultColor]={border:"2px #ccc solid"};
		}
		$scope.xyz[index]={border:"2px #ffc900 solid"};
		defaultColor=index;
	};
	$scope.quiz=function(){
		if(defaultColor>=0){
			quizService.setAnswer({
				quest:$rootScope.q6.input_name,
				answer:$rootScope.questionie[defaultColor].value
			},$scope.q6auth).then(function(res){
				if(res.statusText == 'OK')
				{
					$state.go("q7");
				}
				else
				{
					console.log("wronnnnnng");
				}
			})
		}else{
			alert("Select an answer");
		}
	}
})

.controller('q7Ctrl', function($scope,quizService,$state,$rootScope){
	$rootScope.q7=$rootScope.allquiz[5];
	$rootScope.questionier=$rootScope.allquiz[5].answers;
	for(d=0;d<$rootScope.allquiz[5].answers.length;d++){
		$rootScope.questionier[d].selection=false;
	}
	$scope.q7auth = JSON.parse(localStorage.getItem('authentication'));
	$scope.xyz=[];
	var optAnswer={
		"idx":[0,0,0,0],
		"name":["des_bohemian","des_modern","des_rustic_farmhouse","des_industrial_urban"]
	},add,indexOfMax;
	$scope.answer=function(index){
		if($rootScope.questionier[index].selection){
			$scope.xyz[index]={border:"2px #ccc solid"};
			$rootScope.questionier[index].selection=false;add=-1;
		}else{
			$scope.xyz[index]={border:"2px #ffc900 solid"};
			$rootScope.questionier[index].selection=true;add=1;
		}
		if($rootScope.questionier[index].value=="des_bohemian")
			optAnswer.idx[0]=optAnswer.idx[0]+add;
		else if($rootScope.question[index].value=="des_modern")
			optAnswer.idx[1]=optAnswer.idx[1]+add;
		else if($rootScope.question[index].value=="des_rustic_farmhouse")
			optAnswer.idx[2]=optAnswer.idx[2]+add;
		else
			optAnswer.idx[3]=optAnswer.idx[3]+add;
		indexOfMax = optAnswer.idx.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);
	};
	$scope.quiz=function(){
		if(optAnswer.idx[indexOfMax]>0){
			quizService.setAnswer({
				quest:$rootScope.q7.input_name,
				answer:optAnswer.name[indexOfMax]
			},$scope.q7auth).then(function(res){
				if(res.statusText == 'OK')
				{
					$state.go("q8");
				}
				else
				{
					console.log("wronnnnnng");
				}
			})
		}else{
			alert("Select an answer");
		}
	}
 })
.controller('q8Ctrl', function($scope,quizService,$state,$rootScope){
	$rootScope.q8=$rootScope.allquiz[6];
	$rootScope.qu=$rootScope.allquiz[6].answers;

	$scope.q8auth = JSON.parse(localStorage.getItem('authentication'));
	$scope.xyz=[];
	var defaultColor=-1;
	$scope.answer=function(index){
		if(defaultColor>=0){
			$scope.xyz[defaultColor]={border:"2px #ccc solid"};
		}
		$scope.xyz[index]={border:"2px #ffc900 solid"};
		defaultColor=index;
	};
	$scope.quiz=function(){
		if(defaultColor>=0){
			quizService.setAnswer({
				quest:$rootScope.q8.input_name,
				answer:$rootScope.qu[defaultColor].value
			},$scope.q8auth).then(function(res){
				if(res.statusText == 'OK')
				{
					$state.go("q9");
				}
				else
				{
					console.log("wronnnnnng");
				}
			})
		}else{
			alert("Select an answer");
		}
	}
})
.controller('q9Ctrl', function($scope,quizService,$state,$rootScope){
	$rootScope.q9=$rootScope.allquiz[7];
	$rootScope.qi=$rootScope.allquiz[7].answers;

	$scope.q9auth = JSON.parse(localStorage.getItem('authentication'));
	$scope.xyz=[];
	var defaultColor=-1;
	$scope.answer=function(index){
		if(defaultColor>=0){
			$scope.xyz[defaultColor]={border:"2px #ccc solid"};
		}
		$scope.xyz[index]={border:"2px #ffc900 solid"};
		defaultColor=index;
	};
	$scope.quiz=function(){
		if(defaultColor>=0){
			quizService.setAnswer({
				quest:$rootScope.q9.input_name,
				answer:$rootScope.qi[defaultColor].value
			},$scope.q9auth).then(function(res){
				if(res.statusText == 'OK')
				{
					$state.go("q10");
				}
				else
				{
					console.log("wronnnnnng");
				}
			})
		}else{
			alert("Select an answer");
		}
	}
})
.controller('q10Ctrl', function($scope,quizService,$state,$rootScope){
	$rootScope.q10=$rootScope.allquiz[8];
	$rootScope.qii=$rootScope.allquiz[8].answers;
	for(b=0;b<$rootScope.allquiz[8].answers.length;b++){
		$rootScope.qii[b].buttonSet="button-outline";
	}
	$scope.q10auth = JSON.parse(localStorage.getItem('authentication'));
	var defaultColor=-1;
	$scope.answer=function(index){
		if(defaultColor>=0){
			$rootScope.qii[defaultColor].buttonSet="button-outline";
		}
		$rootScope.qii[index].buttonSet="";
		defaultColor=index;
	};
	$scope.quiz=function(){
		if(defaultColor>=0){
			quizService.setAnswer({
				quest:$rootScope.q10.input_name,
				answer:$rootScope.qii[defaultColor].value
			},$scope.q10auth).then(function(res){
				if(res.statusText == 'OK')
				{
					$state.go("q11");
				}
				else
				{
					console.log("wronnnnnng");
				}
			})
		}else{
			alert("Select an answer");
		}
	}
})
.controller('q11Ctrl', function($scope,quizService,$state,$rootScope){
	$rootScope.q11=$rootScope.allquiz[9];
	$rootScope.questt=$rootScope.allquiz[9].answers;
	$rootScope.questions1=$rootScope.questt;
	$scope.q11auth = JSON.parse(localStorage.getItem('authentication'));
	var key = [],val=[];
 	for(i=0;i<32;i++){
		val[i]=3;
	}
	$scope.answer=function(point, value){
		val[point]=Number(value);
	};
	$scope.quiz=function(){
		quizService.setPersonality({
			"Q1":val[0],"Q2":val[1],"Q3":val[2],"Q4":val[3],"Q5":val[4],
			"Q6":val[5],"Q7":val[6],"Q8":val[7],"Q9":val[8],"Q10":val[9],
			"Q11":val[10],"Q12":val[11],"Q13":val[12],"Q14":val[13],"Q15":val[14],
			"Q16":val[15],"Q17":val[16],"Q18":val[17],"Q19":val[18],"Q20":val[19],
			"Q21":val[20],"Q22":val[21],"Q23":val[22],"Q24":val[23],"Q25":val[24],
			"Q26":val[25],"Q27":val[26],"Q28":val[27],"Q29":val[28],"Q30":val[29],
			"Q31":val[30],"Q32":val[31]
		},$scope.q11auth).then(function(res){
 			if(res.statusText == 'OK')
			{
				$state.go("result");
			}
			else
			{
				console.log("wronnnnnng");
			}
		})
	}
})
.controller('resultCtrl', function($scope,$ionicHistory,quizService,$state,$rootScope,$ionicLoading,$ionicModal){
	$scope.backAcc=function(){
		$ionicHistory.clearCache();
		$ionicHistory.clearHistory();
		$ionicHistory.nextViewOptions({
			disableBack: true
		});
		$state.go("tabsController.account", {}, {reload:true});
	};
	$scope.closeModal = function(){
		$scope.Modal.hide();
		$scope.Modal.remove();
	};
	$scope.openModal = function(read_more) {
		$scope.readmore = read_more;
		showModal($scope, $ionicModal, 'model.html', true);
	};
	$scope.data={};
	$scope.resauth = JSON.parse(localStorage.getItem('authentication'));
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});
	var imgProfile;
	quizService.getResult($scope.resauth).success(function(rest){
		rest.user_image=rest.user_image.replace(/\"/g,"");
		rest.user_image=rest.user_image.replace(/\'/g,"");
		imgProfile=rest.user_image.match(/\?src=https(.*?)\&/);
		if(imgProfile==null){
			imgProfile=rest.user_image.match(/src=https(.*?)\&/);
		}
		rest.user_image="https"+imgProfile[1];

		// main image to show on side
		if(rest.user_selfie_badge != null || rest.user_selfie_badge != undefined){
			rest.user_selfie_badge = rest.user_selfie_badge;
		}else{
			rest.user_selfie_badge = "https://youcoached.com/wp-content/uploads/users_selfie_badges/1/1477983692.png";
		}

		// read more content
		var rm = {};
		rm['read_more'] = rest.user_personality_readmore;
		rest.rm_content = rm;

		// lifestyle coaching content
		var lc = {};
		lc['name'] = 'Lifestyle Coaching';
		lc['color'] = '#ffc40d';
		lc['k'] = 'lifestyle_coaching';
		lc['read_more'] = rest.lifestyle;
		rest.lc = lc;


		$rootScope.reslt=rest;
		$rootScope.quiz=rest.res;
		for(m=0;m<rest.res.length;m++){
			if(m%2==0)
				$rootScope.quiz[m].marginItem="margin-left:10px;";
			else
				$rootScope.quiz[m].marginItem="float:right;margin-right:10px;";
		}
		$ionicLoading.hide();
	});
	$scope.createContact = function(u) {
		$scope.contacts.push({ name: u.firstName + ' ' + u.lastName });
		$scope.modal.hide();
	};
	$scope.toggle = function() {
		$scope.mytxt = !$scope.mytxt;
	};
})
.controller('savedCtrl', function($scope,$ionicHistory,$sce,messageService,saveService,$rootScope,$state,$ionicLoading) {
	$scope.savedauth = JSON.parse(localStorage.getItem('authentication'));
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});
	saveService.getSave($scope.savedauth).success(function(response){
		if(response.messages){
			$scope.messages=response.messages;
			for(r=0;r<response.messages.length;r++){
				$scope.messages[r].clean_title=$sce.trustAsHtml(response.messages[r].message_title);
			}
			$ionicLoading.hide();
		}else{
			$ionicLoading.hide();
		}
	});
	$scope.favorites = function(id){
		messageService.setFavourite({message_id:id},$scope.savedauth).then(function(){
			$state.reload();
		});
	}
})
.controller('pointCtrl', function($scope,$rootScope,$ionicHistory,$sce,rewardService,messageService,$state,$ionicLoading,$ionicModal) {
	$scope.rewardauth = JSON.parse(localStorage.getItem('authentication'));
	$scope.reward=function(){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		rewardService.getReward($scope.rewardauth).success(function(response){
			if(response.status){
				if(response.posts){
					if(response.posts.length > 0){
						$rootScope.re = response;
						$rootScope.point = response.posts;
						for(p=0;p<response.posts.length;p++){
							$rootScope.point[p].clean_title=$sce.trustAsHtml(response.posts[p].post_title);
						}
						$scope.numReward=$rootScope.point.length+" Rewards";
						if(!$rootScope.point.applied_count)
							$rootScope.point.applied_count="";
					}
				}
				$ionicLoading.hide();
			}else{
				$ionicLoading.hide();
			}
		})
	};
	$scope.reward();
	$scope.added = function(sid){
		rewardService.setReward({
			id : sid
		},$scope.rewardauth).success(function(response){
			$scope.reward();
		})
	};
	$scope.favorites = function(id){
		messageService.setFavourite({message_id:id},$scope.rewardauth).then(function(){
			$state.reload();
		});
	};
	$scope.closeModal = function(){
		$scope.Modal.hide();
		$scope.Modal.remove();
	};
	$scope.showMessage = function(reward) {
		$scope.oneshow=[];
		$scope.oneshow.message_title=reward.post_title;
		$scope.oneshow.message_image=reward.image_src;
		var Content=reward.post_content.match(/<img class=\"aligncenter size-full wp-image-2522\"(.*?)\/>/);
		reward.post_content=reward.post_content.replace(Content[0],'<p>&nbsp;</p>'+Content[0]+'<p>&nbsp;</p>');
		$scope.oneshow.message_content=$sce.trustAsHtml(reward.post_content);
		showModal($scope, $ionicModal, 'rewardMessage.html', true);
	}
});

// Find name in (id, name) array
function getCategoryNameById(id, catArray){
    for(var i = 0; i < catArray.length; i++)
    {
        if(catArray[i].term_id == id)
        {
            return catArray[i].name;
        }
    }
}
