utilities = require('./../utilities.js');

module.exports = function ($scope,$window,$ionicHistory,$ionicLoading,$ionicPopup,challengeService) {

  $scope.denyClick = function (challenge) {
    utilities.showLoading($ionicLoading);
    challengeService.denyYChallenge(challenge.id, $auth).success(function(resp){
      utilities.hideLoading($ionicLoading);
      if(!utilities.responseIsOk(resp))
        return;
      challenge.status = 2;
    });
  };

  $scope.skipClick = function (challenge) {
    utilities.showLoading($ionicLoading);
    challengeService.skipYChallenge(challenge.id, $auth).success(function(resp){
      utilities.hideLoading($ionicLoading);
      if(!utilities.responseIsOk(resp))
        return;
      challenge.status = 3;
    });
  };

  $scope.completeClick = function (challenge) {
    utilities.showLoading($ionicLoading);
    challengeService.completeYChallenge(challenge.id, $auth).success(function(resp){
      hideLoading($ionicLoading);
      if(!responseIsOk(resp))
        return;
      challenge.status = 4;
      challenge.completed_users++;
    });
  };

  utilities.showLoading($ionicLoading);

  $auth = JSON.parse(localStorage.getItem('authentication'));

  // TODO: handle if not authenticated
  challengeService.getYChallenges($auth).success(function(resp){
    utilities.hideLoading($ionicLoading);
    $scope.challenges = resp.challenges;
    $scope.challengeCategoryColor = resp.challenge_category_color;
    console.log(resp);
  });

}
