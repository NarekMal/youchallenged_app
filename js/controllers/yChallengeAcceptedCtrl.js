module.exports = function ($scope, $state, $stateParams) {
  $scope.message = $stateParams.message;

  $scope.goBackToChallenge = function () {
    console.log($stateParams);
    $state.go('tabsController.yChallenge', {id: $stateParams.id});
  }

}
