utilities = require('./../utilities.js');

module.exports = function ($scope,$rootScope,$ionicHistory,$ionicLoading,$ionicPopup,$ionicScrollDelegate,$state,$stateParams,challengeService) {

  $auth = JSON.parse(localStorage.getItem('authentication'));

  $scope.passClick = function (challenge) {
    utilities.showLoading($ionicLoading);
    challengeService.passYChallenge(challenge.id, $auth).success(function(resp){
      if(!utilities.responseIsOk(resp)){
        utilities.hideLoading($ionicLoading);
        alert('error');
        return;
      }

      $state.go('tabsController.home');
    });
  };

  $scope.acceptClick = function (challenge) {
    utilities.showLoading($ionicLoading);
    challengeService.acceptYChallenge(challenge.id, $auth).success(function(resp){
      utilities.hideLoading($ionicLoading);
      if(!utilities.responseIsOk(resp.result)){
        if (resp.result == 'limit_reached'){
          $state.go('tabsController.yChallengeLimitReached');
          return;
        }
        alert('error');
        return;
      }
      $state.go('tabsController.yChallengeAccepted', {id: challenge.id, message: resp.accepted_message});
    });
  };

  $scope.completeClick = function (challenge) {
    utilities.showLoading($ionicLoading);
    challengeService.completeYChallenge(challenge.id, $auth).success(function(resp){
      utilities.hideLoading($ionicLoading);
      if(!utilities.responseIsOk(resp)){
        alert('error');
        return;
      }
      challenge.status = 4;
      $rootScope.complYChallengesInCategory++;
      $scope.justCompleted = true;
    });
  };

  $scope.addComment = function(parentCommentId) {
    $scope.comment = {};
    $ionicPopup.show({
      template: '<textarea ng-model = "comment.value"></textarea>',
      title: parentCommentId ? 'Reply to Comment' : 'Add Comment',
      scope: $scope,
      buttons: [{
          text: 'Cancel'
        },
        {
          text: parentCommentId ? '<b>Reply</b>' : '<b>Add</b>',
          type: 'button-positive',
          onTap: function (e) {
            if (!$scope.comment.value) {
              // Don't allow the user to close unless text is entered
              e.preventDefault();
            } else {
              utilities.showLoading($ionicLoading);
              $auth = JSON.parse(localStorage.getItem('authentication'));
              challengeService.addComment($scope.comment.value, $scope.challenge.id, parentCommentId, $auth).success(function(resp){
                if(utilities.responseIsOk(resp))
                  challengeService.getYChallenge($stateParams.id, $auth).success(function(resp){
                    utilities.hideLoading($ionicLoading);
                    $scope.challenge = resp.challenge;
                  });
                else{
                  utilities.hideLoading($ionicLoading);
                  alert('Failed to add comment');
                }
              });
            }
          }
        }
      ]
    });
  }

  $scope.flagComment = function(commentId) {
    $ionicPopup.show({
      template: 'Flag this comment as inappropriate?',
      cssClass: 'popup-without-title',
      buttons: [{
          text: 'Cancel'
        },
        {
          text: '<b>Flag</b>',
          type: 'button-assertive',
          onTap: function (e) {
            utilities.showLoading($ionicLoading);
            $auth = JSON.parse(localStorage.getItem('authentication'));
            challengeService.flagComment(commentId, $auth).success(function(resp){
              utilities.hideLoading($ionicLoading);
              if(utilities.responseIsOk(resp))
                $ionicPopup.alert({
                  template: 'Thank you!<br/>This comment will be sent to a moderator.',
                  cssClass: 'popup-without-title'
                });
              else{
                alert('Failed to flag comment');
              }
            });
          }
        }
      ]
    });
  }

  $scope.scrollToTop = function() {
    $ionicScrollDelegate.scrollTop(true);
  };

  $scope.goBack = function() {
    $ionicHistory.goBack();
  };

  utilities.showLoading($ionicLoading);
  // TODO: handle if not authenticated
  challengeService.getYChallenge($stateParams.id, $auth).success(function(resp){
    console.log(resp);
    utilities.hideLoading($ionicLoading);
    $scope.challenge = resp.challenge;
  });

}
