utilities = require('./../utilities.js');

module.exports = function ($scope,$rootScope,$window,$ionicHistory,$ionicLoading,$ionicPopup,$state,challengeService) {

  $auth = JSON.parse(localStorage.getItem('authentication'));

  $scope.acceptClick = function (challenge) {
    utilities.showLoading($ionicLoading);
    challengeService.acceptYChallenge(challenge.id, $auth).success(function(resp){
      utilities.hideLoading($ionicLoading);
      if(!utilities.responseIsOk(resp.result)){
        if (resp.result == 'limit_reached'){
          $state.go('tabsController.yChallengeLimitReached');
          return;
        }
        alert('error');
        return;
      }
      $state.go('tabsController.yChallengeAccepted', {id: challenge.id, message: resp.accepted_message});
    });
  };

  $scope.passClick = function (challenge) {
    utilities.showLoading($ionicLoading);
    challengeService.passYChallenge(challenge.id, $auth).success(function(resp){
      if(!utilities.responseIsOk(resp)){
        utilities.hideLoading($ionicLoading);
        if (resp == 'limit_reached'){
          $state.go('tabsController.yChallengeLimitReached');
          return;
        }
        alert('error');
        return;
      }
      challengeService.getSuggestedYChallenge($auth).success(function(resp){
        utilities.hideLoading($ionicLoading);
        $scope.challenge = resp.challenge;
        console.log(resp);
      });
    });
  };

  utilities.showLoading($ionicLoading);

  // TODO: handle if not authenticated
  challengeService.getSuggestedYChallenge($auth).success(function(resp){
    $scope.challenge = resp.challenge;
    console.log(resp);
    // Load category info if not already loaded
    if(!$rootScope.yChallengeCategory)
      challengeService.getYChallengeCategoryInfo($auth).success(function(resp){
        $rootScope.yChallengeCategory = resp.category_name;
        $rootScope.yChallengesInCategory = resp.all_challenges;
        $rootScope.complYChallengesInCategory = resp.completed_challenges;
        $rootScope.yChallengeBadgeUrl = resp.category_badge_url;
        utilities.hideLoading($ionicLoading);
      });
    else
      utilities.hideLoading($ionicLoading);
  });
}
