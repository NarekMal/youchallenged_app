utilities = require('./../utilities.js');

module.exports = function ($scope,$window,$rootScope,$ionicLoading,$ionicPopup,$ionicScrollDelegate,$state,challengeService,userService,messagesService) {

  $auth = JSON.parse(localStorage.getItem('authentication'));

  $scope.scrollToTop = function() {
    $ionicScrollDelegate.scrollTop(true);
  };

  $scope.flagComment = function(commentId) {
    $ionicPopup.show({
      template: 'Flag this comment as inappropriate?',
      cssClass: 'popup-without-title',
      buttons: [{
          text: 'Cancel'
        },
        {
          text: '<b>Flag</b>',
          type: 'button-assertive',
          onTap: function (e) {
            utilities.showLoading($ionicLoading);
            $auth = JSON.parse(localStorage.getItem('authentication'));
            challengeService.flagComment(commentId, $auth).success(function(resp){
              utilities.hideLoading($ionicLoading);
              if(utilities.responseIsOk(resp))
                $ionicPopup.alert({
                  template: 'Thank you!<br/>This comment will be sent to a moderator.',
                  cssClass: 'popup-without-title'
                });
              else{
                alert('Failed to flag comment');
              }
            });
          }
        }
      ]
    });
  }

  utilities.showLoading($ionicLoading);
  userService.getAvatar($auth).success(function(resp){
    $scope.avatarHtml = resp;
    messagesService.getMessages($auth).success(function(resp){
      utilities.hideLoading($ionicLoading);
      console.log(resp);
      $scope.challengeMessages = resp;
    });
  });

};
