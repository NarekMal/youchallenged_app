app = require('./app');

app
  // Accepts a timestamp in seconds and returns a string representing how many days have passed since that timestamp
  .filter("daysPassed", function(){
    return function (timeStamp){
      var nowSeconds = Math.floor(Date.now()/1000); // Get seconds from milliseconds
      var secondsInDay = 24*60*60;
      var daysPassed = Math.floor(nowSeconds/secondsInDay) - Math.floor(timeStamp/secondsInDay);
      if(daysPassed == 0)
        return 'TODAY';
      if(daysPassed == 1)
        return 'YESTERDAY';
      return daysPassed+' DAYS AGO';
    };
  });
