module.exports = angular.module('app', ['ionic', 'app.controllers', 'app.directives', 'app.services', 'ngCordova'])
  .constant("Base_URL", "https://youchallengedalpha.tk/wp-json/api")
  .run(function ($ionicPlatform, $state, $ionicHistory) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
      if (localStorage.getItem('authentication') != null) {
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('tabsController.home');
        //$state.go('tabsController.messages');
        //$state.go('login');
        //$state.go('tabsController.account');
        //$state.go('editprofile');
        //$state.go('yChallengeAccepted');
        //$state.go('yChallengeLimitReached');
        //$state.go('tabsController.yChallenges');
        //$state.go('yChallenge', {id: 2903});
        //$state.go('yChallenge', {id: 2905}); //sober
      } else {
        $state.go('intro');
      }
    });
  })
  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist(['self', new RegExp('^(http[s]?):\/\/(w{3}.)?youcoached\.com/.+$'), new RegExp('^(http[s]?):\/\/(w{3}.)?youtube\.com/.+$'), new RegExp('https://player.vimeo.com/video/.+')]);
    $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.navBar.alignTitle('center');
    $stateProvider
      .state('intro', {
        cache: false,
        url: '/intro',
        templateUrl: 'templates/intro.html',
        controller: 'introCtrl'
      })
      .state('login', {
        cache: false,
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
      })
      .state('signup', {
        cache: false,
        url: '/signup',
        templateUrl: 'templates/signup.html',
        controller: 'signupCtrl'
      })
      .state('contest', {
        cache: false,
        url: '/contest',
        templateUrl: 'templates/contest.html',
        controller: 'contestCtrl'
      })
      .state('rules', {
        cache: false,
        url: '/rules',
        templateUrl: 'templates/rules.html',
        controller: 'rulesCtrl'
      })
      .state('forgetpass', {
        cache: false,
        url: '/forgetpass',
        templateUrl: 'templates/forgetpass.html',
        controller: 'forgetpassCtrl'
      })
      .state('contactUs', {
        cache: false,
        url: '/contactUs',
        templateUrl: 'templates/contactUs.html',
        controller: 'contactUsCtrl'
      })
      .state('termsOfService', {
        cache: false,
        url: '/termsOfService',
        templateUrl: 'templates/termsOfService.html',
        controller: 'termsOfServiceCtrl'
      })
      .state('tabsController', {
        cache: false,
        url: '/page1',
        templateUrl: 'templates/tabsController.html',
        abstract: true
      })
      .state('tabsController.yChallenges', {
        cache: false,
        url: '/yChallenges',
        views: {
          'yChallengesView': {
            templateUrl: 'templates/yChallenges.html',
            controller: 'yChallengesCtrl'
          }
        }
      })
      .state('tabsController.home', {
        cache: false,
        url: '/home',
        views: {
          'homeView': {
            templateUrl: 'templates/home.html',
            controller: 'homeCtrl'
          }
        }
      })
      .state('tabsController.account', {
        cache: false,
        url: '/account',
        views: {
          'accountView': {
            templateUrl: 'templates/account.html',
            controller: 'accountCtrl'
          }
        }
      })
      .state('tabsController.messages', {
        cache: false,
        url: '/messages',
        views: {
          'messagesView': {
            templateUrl: 'templates/messages.html',
            controller: 'messagesCtrl'
          }
        }
      })
      .state('tabsController.challenges', {
        cache: false,
        url: '/challenges',
        views: {
          'tab3': {
            templateUrl: 'templates/challenges.html',
            controller: 'challengesCtrl'
          }
        }
      })
      .state('tabsController.saved', {
        cache: false,
        url: '/saved',
        views: {
          'tab4': {
            templateUrl: 'templates/saved.html',
            controller: 'savedCtrl'
          }
        }
      })
      .state('tabsController.point', {
        cache: false,
        url: '/point',
        views: {
          'tab5': {
            templateUrl: 'templates/point.html',
            controller: 'pointCtrl'
          }
        }
      })
      .state('tabsController.yChallenge', {
        cache: false,
        params: {id: null},
        url: '/ychallenge',
        views: {
          'yChallengeView': {
            templateUrl: 'templates/yChallenge.html',
            controller: 'yChallengeCtrl'
          }
        }
      })
      .state('tabsController.yChallengeLimitReached', {
        cache: false,
        url: '/yChallengeLimitReached',
        views: {
          'yChallengeView': {
            templateUrl: 'templates/yChallengeLimitReached.html'
          }
        }
      })
      .state('tabsController.yChallengeAccepted', {
        cache: false,
        params: {id: null, message: null},
        url: '/yChallengeAccepted',
        views: {
          'yChallengeAcceptedView': {
            templateUrl: 'templates/yChallengeAccepted.html',
            controller: 'yChallengeAcceptedCtrl'
          }
        }
      })
      .state('messagecategory', {
        cache: false,
        params: {From: null, param1: null},
        url: '/messagecategory',
        templateUrl: 'templates/messagecategory.html',
        controller: 'messagecategoryCtrl'
      })
      .state('messagetitle', {
        cache: false,
        params: {From: null, param1: null},
        url: '/messagetitle',
        templateUrl: 'templates/messagetitle.html',
        controller: 'messagetitleCtrl'
      })
      .state('completedChallenges', {
        cache: false,
        url: '/completedChallenges',
        templateUrl: 'templates/completedChallenges.html',
        controller: 'completedChallengesCtrl'
      })
      .state('editprofile', {
        cache: false,
        url: '/editprofile',
        templateUrl: 'templates/editprofile.html',
        controller: 'editprofileCtrl'
      })
      .state('changepassword', {
        cache: false,
        url: '/changepassword',
        templateUrl: 'templates/changepassword.html',
        controller: 'changepasswordCtrl'
      })
      .state('q1', {
        cache: false,
        url: '/q1',
        templateUrl: 'templates/q1.html',
        controller: 'q1Ctrl'
      })
      .state('q2', {
        cache: false,
        url: '/q2',
        templateUrl: 'templates/q2.html',
        controller: 'q2Ctrl'
      })
      .state('q3', {
        cache: false,
        url: '/q3',
        templateUrl: 'templates/q3.html',
        controller: 'q3Ctrl'
      })
      .state('q4', {
        cache: false,
        url: '/q4',
        templateUrl: 'templates/q4.html',
        controller: 'q4Ctrl'
      })
      .state('q5', {
        cache: false,
        url: '/q5',
        templateUrl: 'templates/q5.html',
        controller: 'q5Ctrl'
      })
      .state('q6', {
        cache: false,
        url: '/q6',
        templateUrl: 'templates/q6.html',
        controller: 'q6Ctrl'
      })
      .state('q7', {
        cache: false,
        url: '/q7',
        templateUrl: 'templates/q7.html',
        controller: 'q7Ctrl'
      })
      .state('q8', {
        cache: false,
        url: '/q8',
        templateUrl: 'templates/q8.html',
        controller: 'q8Ctrl'
      })
      .state('q9', {
        cache: false,
        url: '/q9',
        templateUrl: 'templates/q9.html',
        controller: 'q9Ctrl'
      })
      .state('q10', {
        cache: false,
        url: '/q10',
        templateUrl: 'templates/q10.html',
        controller: 'q10Ctrl'
      })
      .state('q11', {
        cache: false,
        url: '/q11',
        templateUrl: 'templates/q11.html',
        controller: 'q11Ctrl'
      })
      .state('result', {
        cache: false,
        url: '/result',
        templateUrl: 'templates/result.html',
        controller: 'resultCtrl'
      });
    // Thanks to Ben Noblet!

    $urlRouterProvider.otherwise('/home');

  });

