angular.module('app.services', [])
  .factory('userService', function ($http, Base_URL) {
    return {
      create: function (userdata) {
        return $http.post(Base_URL + "/al_registration_front", userdata, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      resetpassword: function (email) {
        return $http.post(Base_URL + "/al_retrieve_user_pass", email, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      login: function (auth) {
        return $http.get(Base_URL + "/is_user_logged_in", {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      getProfile: function (auth) {
        return $http.get(Base_URL + "/get_account_info", {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      getAvatar: function (auth) {
        return $http.get(Base_URL + "/get_user_avatar", {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      changeYChallengeCategory: function (categoryId, auth) {
        return $http.get(Base_URL + '/change_y_challenge_category', {
          params: {
            'category_id': categoryId
          },
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      registerPlanSubscription: function (subscriptionMethod, auth) {
        return $http.get(Base_URL + '/register_plan_subscription', {
          params: {
            'subscription_method': subscriptionMethod
          },
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      registerAndroidSubscription: function () {
        return $http.get(Base_URL + '/register_android_subscription', {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      setPhoto: function (data, auth) {
        return $http.post(Base_URL + "/save_profile_image", data, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      setProfile: function (data, auth) {
        return $http.post(Base_URL + "/save_profile_front", data, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      setUserNameEmail: function (name, email, auth) {
        return $http.get(Base_URL + "/set_user_name_email", {
          params: {
            'name': name,
            'email': email
          },
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      getAcceptedTOS: function (auth) {
        return $http.get(Base_URL + '/get_accepted_tos', {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      setAcceptedTOS: function (auth) {
        return $http.get(Base_URL + '/set_accepted_tos', {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      handleContactFormData: function (name, email, text, auth) {
        return $http.get(Base_URL + '/handle_contact_form_data', {
          params: {
            'name': name,
            'email': email,
            'text': text
          },
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      }
    }
  })
  .factory('messagesService', function ($http, Base_URL) {
    return {
      getMessages: function (auth) {
        return $http.get(Base_URL + '/get_messages', {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      }
    }
  })
  .factory('homeService', function ($http, Base_URL) {
    return {
      getMessages: function (auth) {
        return $http.get(Base_URL + '/get_users_new_messages', {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      getCamp: function (name, auth) {
        return $http.get(Base_URL + '/get_camp_ids?slug=' + name, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      }
    }
  })
  .factory('messageService', function ($http, Base_URL) {
    return {
      catMessage: function (cn, auth) {
        return $http.get(Base_URL + '/get_camp_ids?slug=' + cn, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      getGoal: function (cn, auth) {
        return $http.get(Base_URL + '/get_self_goals?category=' + cn, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      titleMessage: function (popid, auth) {
        return $http.get(Base_URL + '/get_post_by_id?post_id=' + popid, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      setChallenge: function (data, auth) {
        return $http.post(Base_URL + '/accept_challenge', data, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      setFavourite: function (data, auth) {
        return $http.post(Base_URL + '/save_to_favourite', data, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      addGoal: function (data, auth) {
        return $http.post(Base_URL + "/add_self_goal", data, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      statusGoal: function (data, auth) {
        return $http.post(Base_URL + "/status_self_goal", data, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      deleteGoal: function (data, auth) {
        return $http.post(Base_URL + "/delete_self_goal", data, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      }
    };
  })
  .factory('challengeService', function ($http, Base_URL) {
    return {
      getYChallenges: function (auth) {
        return $http.get(Base_URL + '/get_y_challenges', {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      getSuggestedYChallenge: function (auth) {
        return $http.get(Base_URL + '/get_suggested_y_challenge', {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      getYChallenge: function (challengeId, auth) {
        return $http.get(Base_URL + '/get_y_challenge', {
          params: {
            'challenge_id': challengeId
          },
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      addComment: function (comment, challengeId, parentId, auth) {
        return $http.get(Base_URL + '/add_comment', {
          params: {
            'comment': comment,
            'parent_id': parentId,
            'challenge_id': challengeId
          },
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      flagComment: function (commentId, auth) {
        return $http.get(Base_URL + '/flag_comment', {
          params: {
            'comment_id': commentId
          },
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      acceptYChallenge: function (challengeId, auth) {
        return $http.get(Base_URL + '/accept_y_challenge', {
          params: {
            'challenge_id': challengeId
          },
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      passYChallenge: function (challengeId, auth) {
        return $http.get(Base_URL + '/pass_y_challenge', {
          params: {
            'challenge_id': challengeId
          },
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      completeYChallenge: function (challengeId, auth) {
        return $http.get(Base_URL + '/complete_y_challenge', {
          params: {
            'challenge_id': challengeId
          },
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      getYChallengeCategoryInfo: function (auth) {
        return $http.get(Base_URL + '/get_y_challenge_category_info', {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      getChallenge: function (auth) {
        return $http.get(Base_URL + '/get_challenges', {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      completeChallenge: function (data, auth) {
        return $http.post(Base_URL + '/complete_challenge', data, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      specChallenge: function (auth) {
        return $http.get(Base_URL + '/get_specif_challenges?category=completed', {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      todayChallenge: function (auth) {
        return $http.get(Base_URL + '/get_specif_challenges?category=completed-today', {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      }
    };
  })
  .factory('quizService', function ($http, Base_URL) {
    return {
      getQuestion: function () {
        return $http.get(Base_URL + "/get_quiz_questions", {});
      },
      setAnswer: function (data, auth) {
        return $http.post(Base_URL + "/user_quiz", data, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      setPersonality: function (data, auth) {
        return $http.post(Base_URL + "/personality_quiz", data, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      getResult: function (auth) {
        return $http.get(Base_URL + "/get_quiz_results", {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      }
    };
  })
  .factory('saveService', function ($http, Base_URL) {
    return {
      getSave: function (auth) {
        return $http.get(Base_URL + "/get_saved_messages", {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      }
    }
  })
  .factory('rewardService', function ($http, Base_URL) {
    return {
      getReward: function (auth) {
        return $http.get(Base_URL + "/get_rewards", {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      },
      setReward: function (data, auth) {
        return $http.post(Base_URL + "/apply_reward", data, {
          headers: {
            'Authorization': 'Basic ' + auth,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
          }
        });
      }
    }
  })
  .factory('Camera', function ($q) {
    return {
      getPicture: function (options) {
        var q = $q.defer();
        navigator.camera.getPicture(function (result) {
          q.resolve(result);
        }, function (err) {
          q.reject(err);
        }, options);
        return q.promise;
      }
    }
  })
  .service('UserService', [function () {
    // For the purpose of this example I will store user data on ionic local storage but you should save it on a database
    var setUser = function (authentication) {
      window.localStorage.starter_facebook_user = JSON.stringify(authentication);
    };
    var getUser = function () {
      return JSON.parse(window.localStorage.starter_facebook_user || '{}');
    };
    return {
      getUser: getUser,
      setUser: setUser
    };
  }]);
